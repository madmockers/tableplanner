<?php
session_start();

if($_SESSION["authed"] == 1)
{
	$eventId = 0;

	$teams = json_decode(stripslashes($_POST["teams"]));
	$taken = json_decode(stripslashes($_POST["taken"]));

	$file = fopen("table_layout.json", "w");

	$output["teams"] = $teams;
	$output["taken"] = $taken;

	fwrite($file, json_encode($output));

	fclose($file);

	$return["success"] = true;
	$return["msg"]["msg"] = "Save Successful!";
	$return["msg"]["color"] = "green";
}
else
{
	$return["success"] = false;
	$return["msg"]["msg"] = "You are not authed!";
	$return["msg"]["color"] = "red";
}

echo json_encode($return);
?>