<?php
session_start();

$fileName = "table_layout.json";

if($_SESSION["authed"] == 1)
{

	if(file_exists($fileName))
	{
		$file = fopen($fileName, "r");

		$data = json_decode(fread($file, filesize($fileName)), true);

		$data["exists"] = true;

		fclose($file);
	}
	else
	{
		$data["exists"] = false;
	}
	$data["authed"] = true;
}
else
{
	$data["authed"] = false;
}

echo json_encode($data);
?>