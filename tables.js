/**
 * Author: Glen 'MadMockers' Chatfield
 * Bugs: floorplanner@madmockers.com
 */

Object.size = function(obj)
{
    var size = 0, key;
    for(key in obj)
        if(obj.hasOwnProperty(key)) size++;
    return size;
}

var selected = {};

//var taken = {"A1" : true};
var allowed = {};

var memberCounter = 0;
var validMembers = 0;

var readOnly = false;

var LAYOUT_ZIGZAG = 0
var LAYOUT_DOWN_SIDES = 1

var layout = LAYOUT_DOWN_SIDES;
/* choose 1 */
/* zigzag */
if(layout == LAYOUT_ZIGZAG)
{
    var sectionsId = {
        'A' : 0, 'B' : 1, 'C' : 2, 'D' : 3, 'E' : 4, 'F' : 5, 'G' : 6, 'H' : 7, 'I' : 8, 'J' : 9,
        'K' :10, 'L' :11, 'M' :12, 'N' :13, 'O' :14, 'P' :15, 'Q' :16, 'R' :17, 'S' :18, 'T' :19 }
    var sections = [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 
        'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T' ]
}

/* down sides */
else if(layout == LAYOUT_DOWN_SIDES)
{
    var sectionsId = {
        'A' : 0, 'J' : 1, 'B' : 2, 'K' : 3, 'C' : 4, 'L' : 5, 'D' : 6, 'M' : 7, 'E' : 8, 'N' : 9,
        'F' :10, 'O' :11, 'G' :12, 'P' :13, 'H' :14, 'Q' :15, 'I' :16, 'R' :17 }
    var sections = [
        'A', 'J', 'B', 'K', 'C', 'L', 'D', 'M', 'E', 'N', 
        'F', 'O', 'G', 'P', 'H', 'Q', 'I', 'R' ]
}

var currentTeamIdx = 0;
var currentTeam = null;

var teamIdCounter = 1;

var teams = null;
var taken = null;

function saveLocally()
{
    localStorage.setItem('teams', JSON.stringify(teams));
    localStorage.setItem('taken', JSON.stringify(taken));
    localStorage.setItem('localData', 'true');
}

function load()
{
    if(getUrlVars()["readonly"] == "no")
    {
        readOnly = false;
        localStorage.setItem('authed', 'false');
    }
    else
    {
        readOnly = true;
        $(".readonly").css("display", "none");
        $("#teamName").attr("readonly", true);
    }

    teams = [];
    taken = {};
    if(!readOnly)
    {
        if(localStorage.getItem('localData') == 'true')
        {
            message("Using Locally Stored Data!", "green");
            teams = JSON.parse(localStorage.getItem('teams'));
            taken = JSON.parse(localStorage.getItem('taken'));

            currentTeam = teams[0];

            setTaken();
            showOverlay();
            updateTeamIdCounter();

            $("#teams").html(createTeams());
        }
        else
        {
            teams.push({
                "id"        : 0,
                "name"        : "None",
                "members"    : null
            });
            $.getJSON("getState.php", "", function (data) 
                {
                    if(data.authed)
                    {
                        authed();
                        message("Loaded Layout", "green");
                        if(data.exists)
                        {
                            teams = data.teams;
                            taken = data.taken;

                            if(Object.size(taken) == 0)
                                taken = {};

                            updateTeamIdCounter();
                            
                            for(teamIdx in teams)
                            {
                                for(memberIdx in teams[teamIdx].members)
                                {
                                    if(typeof teams[teamIdx].members[memberIdx] === "string")
                                    {
                                        member = { "ref": teams[teamIdx].members[memberIdx], "alias": "Invalid Reference" }
                                        teams[teamIdx].members[memberIdx] = member
                                    }
                                }
                            }
                        }
                        setCurrentTeam(0);

                        $("#teams").html(createTeams());
                    }
                    else
                    {
                        message("You are not authed!", "red");
                    }
                });
        }
    }
    else
    {
        $.getJSON("getReducedState.php", "", function (data) 
                {
                    if(data.exists)
                    {
                        teams = data.teams;
                        taken = data.taken;

                        if(Object.size(taken) == 0)
                            taken = {};

                        updateTeamIdCounter();
                    }
                    
                    var team = getUrlVars()["team"];
                    if(team != null)
                        setCurrentTeam(team);
                    else
                        setCurrentTeam(0);

                    $("#teams").html(createTeams());
                });
    }
}

function updateTeamIdCounter()
{
    var id = 0;
    for(teamIdx in teams)
    {
        if(teams[teamIdx].id > id)
            id = teams[teamIdx].id;
    }
    teamIdCounter = id + 1;
}

function reload()
{
    localStorage.setItem('localData', 'false');
    load();
}

function authed()
{
    $("#pass").css("display", "none");
    localStorage.setItem('authed', 'true')
}

function auth()
{
    var e = $("#auth");

    $.post("auth.php", "pass=" + e.val(), function(data)
        {
            if(data.success)
            {
                message("Successfully Authed!", "green");
                load();
            }
            else
            {
                message("Invalid Password!", "red");
            }
        }, "json");

    e.val("");
}

function save()
{
    var query = "teams=" + encodeURIComponent(JSON.stringify(teams)) + 
                "&taken=" + encodeURIComponent(JSON.stringify(taken));

    console.log("Save Query: " + query);

    $.post("saveChanges.php", query, function (data)
        {
            if(data.success)
            {
                localStorage.setItem("localData", false);
            }
            message(data.msg.msg, data.msg.color);
        }, "json");
}

function message(msg, color)
{
    $("#msg").css("color", color);
    $("#msg").html(msg);
}

var validateReturned = 0;

function validate()
{
    var query = "count=" + memberCounter;
    for(var i = 0;i < memberCounter;i++)
    {
        query += "&user" + i + "=" + encodeURIComponent($.trim($("#ref" + i).val()));
    }
    $.getJSON("validateUsers.php", query, function (data)
        {
            validMembers = 0;
            members = [];
            if(data == null)
                return;

            currentTeam.isInvalid = false;
            for(var i = 0;i < data.length;i++)
            {
                member = {};
                member["valid"] = false;
                var e = $("#ref" + i);
                e.removeClass();
                if(data[i].found)
                {
                    if(data[i].paid)
                    {
                        e.addClass("valid");
                        validMembers++;
                        e.attr("readonly", true);
                        e.val(data[i].ref);
                        member["alias"] = data[i].alias;
                        member["valid"] = true;
                    }
                    else
                    {
                        e.addClass("invalid");
                        e.attr("readonly", true);
                        currentTeam.isInvalid = true;
                        e.val(data[i].ref + " (Unpaid)");
                        member["alias"] = data[i].alias + " (No Ticket)";
                    }
                }
                else
                {
                    e.addClass("invalid");
                    currentTeam.isInvalid = true;
                    e.attr("readonly", false);
                    member["alias"] = "Invalid Reference"
                }
                member["ref"] = e.val();
                members.push(member);
            }
            teams[currentTeamIdx]["members"] = members;
            saveLocally();
            createTeams();
            validateReturned = 1;
        });
}

function finalize()
{
    readOnly = true;
    clearAllowed();
}

function clearAllowed()
{
    for(seat in allowed)
    {
        var e = $("#" + seat);
        e.removeClass("allowed");
    }

    allowed = {};
}

function clearSelected()
{
    for(seat in selected)
    {
        var e = $("#" + seat);
        e.removeClass("selected");
    }

    selected = {};
}

function getTeamIdx(teamId)
{
    for(teamIdx in teams)
        {
                if(teams[teamIdx].id == teamId)
                {
            return teamIdx;
                }
        }
    return -1;
}

function setCurrentTeam(teamId)
{
    for(teamIdx in teams)
    {
        if(teams[teamIdx].id == teamId)
        {
            currentTeamIdx = teamIdx;
            currentTeam = teams[teamIdx];
            break;
        }
    }

    clearMembers();

    $("#teamName").val("");
    $("#contactEmail").val("");

    if(teamId != 0)
    {
        for(teamIdx in teams)
        {
            team = teams[teamIdx];
            if(team["id"] == teamId)
            {
                $("#teamName").val(team["name"]);
                
                    members = team["members"];
                    for(memberIdx in members)
                    {
                        if(readOnly)
                            member = members[memberIdx]["alias"]
                        else
                            member = members[memberIdx]["ref"]
                        element = $(addMember(member));
                        if(readOnly)
                        {
                            element.attr("readonly", true);
                            if(!members[memberIdx]["valid"])
                            {
                                element.addClass("invalid");
                            }
                        }
                            
                    }
                if(!readOnly)
                {
                    $("#contactEmail").val(team["email"]);
                }
                break;
            }
        }
        $("#teamCreate").css("display", "none");
        $("#teamEdit").css("display", "block");
    }
    else
    {
        $("#teamCreate").css("display", "block");
        $("#teamEdit").css("display", "none");
    }

    if(overlayOn)
        showOverlay()

    if(!readOnly)
        validate();

    setTaken();
}

function setTaken()
{
    clearSelected();
    clearAllowed();
    for(seat in taken)
    {
        var e = $("#" + seat);
        
        e.removeClass("taken selected");

        if(taken[seat] != currentTeam.id)
            e.addClass("taken");
        else
        {
            e.addClass("selected");
            selected[seat] = true;

            var section = seat.charAt(0);
            var numb = parseInt(seat.substring(1))
            
            var row = Math.floor(numb / 24);
            var col = Math.floor(numb % 24);
            
            if(!readOnly)
            {
                for(var x = -1;x <= 1;x++)
                {
                    for(var y = -1;y <= 1;y++)
                    {
                        if(Math.abs(x) == Math.abs(y))
                            continue;

                        testSeat = getIdFromSection(section, row + x, col + y);

                        if(taken[testSeat] != null || testSeat in selected)
                            continue;

                        if(testSeat != null && isAllowed(testSeat))
                        {
                            $("#" + testSeat).addClass("allowed");
                            allowed[testSeat] = true;
                        }
                    }
                }
            }
        }
    }
}

function selectSeat(elem)
{
    if(elem == null)
        return
    //console.log("1: " + Object.size(selected))
    var e = $("#" + elem.id);
    
    if(isTaken(elem.id))
    {
        setCurrentTeam(taken[elem.id]);
        return;
    }

    if(readOnly)
        return

    if(elem.id in selected)
    {
        //console.log("2: " + Object.size(selected))
        if(!canRemove(elem.id))
            return;
        //console.log("3: " + Object.size(selected))
        delete selected[elem.id];
        delete taken[elem.id];
        e.removeClass("selected");
        
        var section = elem.id.charAt(0);
        var numb = parseInt(elem.id.substring(1))
        
        var row = Math.floor(numb / 24);
        var col = Math.floor(numb % 24);
        
        for(var x = -1;x <= 1;x++)
        {
            for(var y = -1;y <= 1;y++)
            {
                if(Math.abs(x) == Math.abs(y))
                    continue;
                testSeat = getIdFromSection(section, row + x, col + y);
                if(testSeat != null && !isAllowed(testSeat))
                {
                    $("#" + testSeat).removeClass("allowed");
                    delete allowed[testSeat];
                }
            }
        }
        
        if(Object.size(selected) > 0)
        {
            e.addClass("allowed");
            allowed[elem.id] = true;
        }
    }
    else
    {
        var allow = false;
        if(currentTeam.id == 0)
        {
            allow = false;
            message("Select a team or create a new team to allocate seats", "red");
        }
        else
        {
            if(Object.size(selected) == 0)
                allow = true;
            else if(elem.id in allowed)
                allow = true;
            
            if(isTaken(elem.id))
                allow = false;

            if(validMembers <= Object.size(selected))
            {
                allow = false;
                if(validMembers > 0) alert("You have already booked all validated members!");
                else alert("You need to validate member reference numbers first!");
            }
        }
        
        if(allow)
        {
            var section = elem.id.charAt(0);
            var numb = parseInt(elem.id.substring(1))
            
            var row = Math.floor(numb / 24);
            var col = Math.floor(numb % 24);
            
            for(var x = -1;x <= 1;x++)
            {
                for(var y = -1;y <= 1;y++)
                {
                    if(Math.abs(x) == Math.abs(y))
                        continue;
                    allowedSeat = getIdFromSection(section, row + x, col + y);
                    if(allowedSeat != null && !selected[allowedSeat] && !taken[allowedSeat])
                    {
                        allowed[allowedSeat] = true;
                        $("#" + allowedSeat).addClass("allowed");
                    }
                }
            }
            
            selected[elem.id] = true;
            taken[elem.id] = currentTeam.id;
            delete allowed[elem.id];
            e.removeClass("allowed");
            e.addClass("selected");
        }
    }
    saveLocally();
}

function isTaken(id)
{
    return taken[id] != currentTeam.id && taken[id] != null;
}

function isAllowed(id)
{
    if(id == null || id == NaN)
        return false;

    var section = id.charAt(0);
    var numb = parseInt(id.substring(1))
    
    var row = Math.floor(numb / 24);
    var col = Math.floor(numb % 24);
    
    for(var x = -1;x <= 1;x++)
    {
        for(var y = -1;y <= 1;y++)
        {
            if(Math.abs(x) == Math.abs(y))
                continue;
            seat = getIdFromSection(section, row + x, col + y);
            if(selected[seat])
                return true;
        }
    }
    return false;
}

//function getSectionOffset(id)
//{
//    /* choose one */
//
//    /* letters down the sides instead of zigzagging */
//    if(0)
//    {
//        sectHeight = sections.length / 2;
//        return [ id / sectHeight, id / 2 ] 
//    }
//    /* zigzagging letters */
//    if(1)
//    {
//        return [ id % 2, id / 2 ] 
//    }
//}

function getIdFromSection(letter, row, col)
{
    var sectId = sectionsId[letter];

    if(col < 0 || col >= 24)
        return null;
    
    if(row < 0)
    {
        if(sectId <= 1)
            return null;
        letter = sections[sectId - 2];
        row += 2;        
    }
    else if(row >= 2)
    {
        if(sectId >= sections.length - 2)
            return null;
        letter = sections[sectId + 2];
        row -= 2;
    }
    
    var id = row * 24 + col;
    return letter + id;
}

function getId(col, row)
{
    if(col < 0 || col >= 48 || row < 0)
        return null;

    letterOffs = 0;
    if(col >= 24)
        letterOffs = 1;
    letterOffs += 2 * Math.floor(row / 2);
    
    if(letterOffs >= sections.length)
        return null;
        
    letter = sections[letterOffs];
    
    return letter + ((col % 24) + (row % 2) * 24);
}

function getCoord(id)
{
    var sectionId = sectionsId[id.charAt(0)];
    var numb = parseInt(id.substring(1))
    
    var row = Math.floor(numb / 24);
    var col = Math.floor(numb % 24);
    
    row += 2 * (sectionId >> 1)
    if((sectionId & 1) > 0)
        col += 24;
    
    return [col, row];
}

function canRemove(id)
{
    //console.log(selected);
    sCoords = getCoord(id);
    
    //console.log("Starting Coords: " + sCoords);

    openNodes = [];
    closedNodes = {};
    closedNodes[id] = true;
    
    // find starting node:
    {
        var sur = getSurrounding(sCoords);
        for(s in sur)
        {
            if(selected[getId(sur[s][0], sur[s][1])])
            {
                openNodes.push(sur[s]);
                break;
            }
        }
    }
    
    while(openNodes.length > 0)
    {
        //console.log("----------------------- NEW ROUND ---------------------------");
        //console.log("Open Node Count: " + openNodes.length);
        //console.log(openNodes);
        //console.log("Closed Node Count: " + Object.size(closedNodes));
        //console.log(closedNodes);
    
        newNodes = [];
        newNodeIds = {};
        
        for(nodeNum in openNodes)
        {
            node = openNodes[nodeNum];
            //console.log(node);
            surrounding = getSurrounding(node);
            //console.log(surrounding);
            for(newNodeNum in surrounding)
            {
                var newNode = surrounding[newNodeNum];
                nodeId = getId(newNode[0], newNode[1]);
                if(nodeId != null && !(nodeId in closedNodes) && selected[nodeId] && !newNodeIds[nodeId])
                {
                    newNodes.push(newNode);
                    newNodeIds[nodeId] = true;
                    //console.log("New Node: X: " + newNode[0] + "; Y: " + newNode[1] + "; ID: " + nodeId);
                }
            }
            closedNodes[getId(node[0], node[1])] = true;
        }
        openNodes = newNodes;
    }
    //console.log("------ RESULT(" + Object.size(closedNodes) + " == " + Object.size(selected) + ") ------");
    return Object.size(closedNodes) == Object.size(selected);
}

function getSurrounding(coords)
{
    var output = []
    for(var x = -1;x <= 1;x++)
    {
        for(var y = -1;y <= 1;y++)
        {
            if(Math.abs(x) == Math.abs(y))
                continue;
            
            var node = new Array(parseInt(coords[0] + x), parseInt(coords[1] + y));
            output.push(node);
        }
    }
    return output;
}

function createNewTeam()
{
    var name = $("#teamName").val();
    var email = $("#contactEmail").val();

    var team = {
        "id"        : teamIdCounter,
        "name"        : name,
        "email"        : email,
        "count"        : 0,
        "members"    : null
    };

    members = [];
    for(var i = 0;i < memberCounter;i++)
    {
        members.push( { "ref": $("#ref" + i).val(), "alias": "Invalid Reference" } );
    }

    team.members = members;

    teams.push(team);
    saveLocally();

    setCurrentTeam(teamIdCounter); 

    teamIdCounter++;

    $("#teams").html(createTeams());

    message("Team Created", "green");
}

function quickCreate()
{
    var e = $("#quickCreate");

    var text = e.val();
    e.val('');

    text = text.split("\t");

    var name = text[1];
    var email = text[3];

    var members = [];

    var i = 6;
    while(true)
    {
        var member = text[i];
        if(member == null || member == "")
            break;
        members.push( { "ref": member, "alias": "Invalid Reference" } );
        i++;
    }

    var team = {
        "id"        : teamIdCounter,
        "name"        : name,
        "email"        : email,
        "count"        : 0,
        "members"    : members
    };

    teams.push(team);
    saveLocally();

    setCurrentTeam(teamIdCounter); 

    teamIdCounter++;

    $("#teams").html(createTeams());

    message("Team Created", "green");
}

function saveTeam()
{
    var team = currentTeam;

    team["name"] = $("#teamName").val();
    team["email"] = $("#contactEmail").val();

    saveLocally();

    message("Team Saved", "green");

    $("#teams").html(createTeams());
}

function deleteTeam()
{
    for(seat in taken)
    {
        if(taken[seat] == currentTeam.id)
        {
            $("#" + seat).removeClass("taken");
            delete taken[seat];
        }
    }

    teams.splice(currentTeamIdx, 1);
    setCurrentTeam(0);

    message("Team Deleted", "green");

    saveLocally();

    $("#teams").html(createTeams());
}

function addMember(ref)
{
    str = "<input id=\"ref" + memberCounter + "\" value=\"" + ref + "\">"
    if(!readOnly)
    {
        str = "<br>RFLAN Reference Number: " + str;
        str += "<img src=\"red_x.png\" onclick=\"deleteMember(" + memberCounter + ")\">";
    }
    else
        str = "<br>Alias: " + str;
    $("#refs").append(str);
    memberCounter++;
    return document.getElementById("ref" + (memberCounter - 1));
}

function deleteMember(id)
{
    if(Object.size(currentTeam.members) <= Object.size(selected))
    {
        message("Remove seats before removing team members!", "red");
        return;
    }
    currentTeam.members.splice(id, 1);
    setCurrentTeam(currentTeam.id);
    saveLocally();
}

function clearMembers()
{
    memberCounter = 0;
    $("#refs").html("");
}

function createTables()
{
    document.write("<table cellspacing=\"0\" cellpadding=\"0\"><tr><td colspan=\"8\" align=\"center\">Stage</td></tr>") //<tr><td class=\"section-cell\">A</td><td style=\"padding-right: " + gap + "px\">");
    for(l in sections)
    {
        createTableGrid(sections[l]);
    }
    //createTableGrid("A");
    //document.write("</td><td style=\"padding-left: " + gap + "px\">");
    //createTableGrid("B");
    //document.write("</td><td class=\"section-cell\">B</td></tr><tr><td class=\"section-cell\">C</td><td style=\"padding-right: " + gap + "px\">");
    //createTableGrid("C");
    //document.write("</td><td style=\"padding-left: " + gap + "px\">");
    //createTableGrid("D");
    //document.write("</td><td class=\"section-cell\">D</td></tr><tr><td class=\"section-cell\">E</td><td style=\"padding-right: " + gap + "px\">");
    //createTableGrid("E");
    //document.write("</td><td style=\"padding-left: " + gap + "px\">");
    //createTableGrid("F");
    //document.write("</td><td class=\"section-cell\">F</td></tr><tr><td class=\"section-cell\">G</td><td style=\"padding-right: " + gap + "px\">");
    //createTableGrid("G");
    //document.write("</td><td style=\"padding-left: " + gap + "px\">");
    //createTableGrid("H");
    //document.write("</td><td class=\"section-cell\">H</td></tr><tr><td class=\"section-cell\">I</td><td style=\"padding-right: " + gap + "px\">");
    //createTableGrid("I");
    //document.write("</td><td style=\"padding-left: " + gap + "px\">");
    //createTableGrid("J");
    //document.write("</td><td class=\"section-cell\">J</td></tr><tr><td colspan=\"4\" align=\"center\">Entrance</td></tr></table>");
    document.write("<tr><td colspan=\"8\" align=\"center\">Entrance</td></tr></table>")
    
}

function createTableGrid(letter)
{
    var id = sectionsId[letter]
    var gap = 60;
    document.write("<!-- New Letter: " + letter + " -->")
    if(id % 2 == 0)
    {
        document.write("<tr>");
    }
    else
    {
        document.write("<td style=\"padding-left: " + gap + "px\"></td>")
    }
    document.write("<td style=\"padding-left:50px\" class=\"section-cell\">" + letter + "</td>")
    document.write("<td>")
    if(id == 0)
    {
        document.write("<span id=\"gridStart-wing0\" style=\"width: 0px; height: 0px; border: 0px; margin: 0px; padding: 0px\"></span>");
    }
    else if(id == 1)
    {
        document.write("<span id=\"gridStart-wing1\" style=\"width: 0px; height: 0px; border: 0px; margin: 0px; padding: 0px\"></span>");
    }
    document.write("<div class=\"section-container\" id=\"" + letter + "\" style=\"position: relative;\">");
    document.write("<div id=\"set-1\" style=\"position: relative; height: 40px; margin-bottom: 40px;\">");
    createSet(letter, 1);
    document.write("</div>");
    
/*    document.write("<div id=\"set-2\" style=\"position: relative; height: 40px; margin-bottom: 40px;\">");
    createSet(letter, 37);
    document.write("</div>"); */
    document.write("</div>");

    document.write("</td>");
    document.write("<td style=\"padding-left:50px\" class=\"section-cell\">" + letter + "</td>");
    if(id % 2 == 0)
    {
        document.write("<td style=\"padding-right: " + gap + "px\"></td>")
    }
    else
    {
        document.write("</tr>");
    }
}

function createSet(letter, start)
{
    for(var i = 0;i < 48;i++)
    {
        var top = Math.floor(i / 24) * 20;
        var left = i % 24 * 20;
        document.write("<div onclick=\"selectSeat(this)\" class=\"seat unselectable\" unselectable=\"on\" id=\"" + letter + (i + start - 1) + "\" style=\"position: absolute; top: " + top + "; left: " + left + ";\">" + (i + start) + "</div>");
    }
}

function displayTeams()
{
    $("#teams").html(createTeams());
}

function createTeams()
{
    var count = {};

    var filter = $("#filter").val()
    if(filter == null)
        filter = ""
    filter = filter.toLowerCase()

    var doFilter = filter != "";

    console.log(doFilter + ": " + filter);

    for(seat in taken)
    {
        if(count[taken[seat]] == null)
            count[taken[seat]] = 1;
        else
            count[taken[seat]]++;
    }

    var output = "";
    for(teamIdx in teams)
    {
        team = teams[teamIdx];
        if(team == null)
            continue;

        var invalid = false;
        if((readOnly || team.id != 0) && (team.isInvalid || (!readOnly && Object.size(team.members) != count[team.id])))
            invalid = true;

        if(doFilter && team.name.toLowerCase().indexOf(filter) == -1)
            continue;

        output += "<input type=\"button\" onclick=\"setCurrentTeam(" + team.id + 
            ")\" value=\"" + team.name + (readOnly?"":" (" + team.id + ")") + "\""
        if(invalid)
            output += " class=\"invalid\"";

        output += ">";
    }
    return output;
}

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function generateOverlay(wing, x, y)
{
    var html = "<div id=\"overlay-wing" + wing + "\" style=\"position: absolute; z-index: 10; left: " + x + "px; top: " + y + "px;\">";
        
//    for(var wing = 0;wing < 2;wing++)
    {
//        console.log("Starting wing " + wing);
        for(var row = 0;row < 18;row++) // 20 rows in each wing
        {
//            console.log("\tRow: " + row);
            var curTId = 0;
            var startCol = 0;
            for(var col = 0;col <= 24;col++)
            {
                var realCol = col + 24 * wing;
                
                var cellTId = 0;
                if(col != 24)
                {
                    var id = getId(realCol, row)
                    if(taken[id] != null)
                        cellTId = taken[id];
//                    console.log("\t\t" + id + " (" + cellTId + "=?" + curTId + ")")
                }

                if(cellTId != curTId || col == 24)
                {
                    if(curTId != 0 || (col == 24 && cellTId != 0))
                    {
                        /*if(col == 18 && curTId == 0)
                        {
                            startCol = 17;
                            curTId = cellTId;
                        }*/
                        // generate output

                        var team = teams[getTeamIdx(curTId)];
                        if(team.id != currentTeam.id)
                        {
                            //console.log("Generating " + startCol + "->" + col + " on row " + row + " for team " + team.name);
                            var top = row * 40 - 20 * (row & 1);
                            var left = (startCol%24) * 20; // + wing * (18 * 20 + 250);
                            var styles = "height: 19px; width: " + ((realCol - startCol) * 20 - 1) + 
                                "px; background-color: #555555; border-width: 1px; border-style: solid; " + 
                                "border-color: #000000; top: " + top + "px; left: " + left + "px; " + 
                                "text-align: center; position: absolute; overflow: hidden; color: #FFFFFF; " +
                                "white-space: nowrap; font-size: 12px; line-height: 19px";
                            html += "<div style=\"" + styles + "\" onclick=\"setCurrentTeam(" + team.id + ")\">" + team.name + "</div>";
                        }
                    }
                    startCol = realCol;
                    curTId = cellTId;
                }
            }
        }
    }
    html += "</div>"
    return html;
}

function getOffset( el ) {
    var _x = 0;
    var _y = 0;
    while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
        _x += el.offsetLeft;
        _y += el.offsetTop;
        el = el.offsetParent;
    }
    return { x: _x, y: _y };
}

function showOverlay()
{
    var container = document.getElementById("overlayContainer");
    container.innerHTML = ""
    for(var wing = 0;wing < 2;wing++)
    {
        var gridStart = document.getElementById("gridStart-wing" + wing);

        var c1 = getOffset(gridStart);
        var c2 = getOffset(container);
        var coords = {
            x: c1["x"]-c2["x"],
            y: c1["y"]-c2["y"]
        };
        container.innerHTML += generateOverlay(wing, coords["x"], coords["y"]);
    }
}

var overlayOn = true;

function toggleOverlay()
{
    overlayOn = !overlayOn;
    if(overlayOn)
    {
        showOverlay();
    }
    else
    {
        document.getElementById("overlayContainer").innerHTML = "";
    }
}

function getTeamMembers(teamId)
{
    return teams[getTeamIdx(teamId)].members;
}

function findDuplicateBookings()
{
    var userList = {};
    var dups = [];
    for(teamIdx in teams)
    {
        if(teams[teamIdx].id == 0)
            continue;
        for(memberIdx in teams[teamIdx].members)
        {
            ref = teams[teamIdx].members[memberIdx]["ref"];
            if(ref in userList)
            {
                dups.push([ref, teams[teamIdx]]);
            }
            else
            {
                userList[ref] = teams[teamIdx];
            }
        }
    }
    for(dupIdx in dups)
    {
        dups[dupIdx].push(userList[dups[dupIdx][0]]);
    }
    return dups;
}

function validateAll(a) {
    if(a == 0)
    {
        validateReturned = 0
        setCurrentTeam(teams[1].id)
        setTimeout("validateAll(1)", 10)
        return
    }
    if(a == Object.size(teams)) return;
    if(validateReturned == 0)
    {
        setTimeout("validateAll(" + (a) + ")", 10);
        return;
    }
    saveTeam();
    validateReturned = 0;
    setCurrentTeam(teams[a].id);
    setTimeout("validateAll(" + (a+1) + ")", 10);
}
