<?php


$fileName = "table_layout.json";

$reducedData;

if(file_exists($fileName))
{
	$file = fopen($fileName, "r");

	$data = json_decode(fread($file, filesize($fileName)), true);

	$reducedData["teams"] = array();

	foreach($data["teams"] as $team)
	{
		$t["id"] = $team["id"];
		$t["name"] = $team["name"];
		array_push($reducedData["teams"], $t);
	}

	$reducedData["taken"] = $data["taken"];

	$reducedData["exists"] = true;

	fclose($file);
}
else
{
	$data["exists"] = false;
}


echo json_encode($reducedData);
?>