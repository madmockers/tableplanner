<?php session_start() ?>
<!--
Author: Glen 'MadMockers' Chatfield
Bugs: floorplanner@madmockers.com
-->
<html>
	<head>
		<title>Seating Selection</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script src="tables.js"></script>
		<LINK REL=StyleSheet HREF="styles.css" TYPE="text/css" MEDIA=screen>
	</head>
	<body onload="load()">
		<div id="msg">
		</div>
		<div>
			<table>
				<tr>
					<td valign="top" width="1200" style="padding: 50px">
						<div id="overlayContainer" style="position: relative;"></div>
						<script type="text/javascript">
						createTables();
						</script>
					</td>
					<td valign="top">
						<div>
							<div id="pass" class="readonly">
								<input placeholder="Password" id="auth" type="password"><input type="button" value="Auth" onclick="auth()">
							</div>
							<div id="info">
								<table>
									<tr class="light">
										<td class="formLabel">Team Name:</td><td><input id="teamName"></td>
									</tr>
									<tr class="dark readonly">
										<td class="formLabel">Contact Email:</td><td><input id="contactEmail"></td>
									</tr>
									<tr class="light">
										<td class="formLabel">Team Members:</td>
										<td>
											<div class="readonly">
												<input type="button" onclick="validate()" value="Validate"><input type="button" onclick="addMember('')" value="Add Member"><br>
											</div>
											<div id="refs" class="refs">
											</div>
										</td>
									</tr>
									<tr class="dark readonly">
										<td></td>
										<td style="position: relative;">
											<div id="submit">
												<div id="teamCreate">
													<input id="quickCreate"><input type="button" onclick="quickCreate()" value="Quick Create"><br/>
													<input type="button" id="createButton" onclick="createNewTeam()" value="Create Team">
												</div>
												<div id="teamEdit" style="display: none">
													<input type="button" onClick="saveTeam()" value="Save Team">
													<input type="button" onClick="deleteTeam()" value="Delete Team">
													<input type="button" onClick="setCurrentTeam(0)" value="New Team">
												</div>
											</div>
										</td>
									</tr>
								</table>
							</div>
							<h2>Teams</h2>
							<input id="filter" onkeyup="displayTeams()" placeholder="Filter"><input type="button" value="Toggle Team Names" onclick="toggleOverlay()">
							<div id="teams">
							</div>
							<div class="readonly">
								<h2>Save</h2>
								<input type="button" onclick="save()" value="Submit Changes">
								<br>
								<br>
								<br>
								<input type="button" onclick="reload()" value="Discard Changes (Reload State)">
							</div>
							<div style="margin-top: 50px; border-width: 2px; border-color: red; border-style: solid; padding: 10px;">
<b>Team bookings are processed by an actual human. Please be patient for changes to be made.</b><br/>
If your team is shown as red above, there was a problem with the group booking. Click on it to see more information.<br/>
If you submitted an incomplete booking, and you don't see it above, it has probably been ignored.
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>
